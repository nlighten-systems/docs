# SCTools

The `sctools` utility program is used to test sending and receiving packets and to download SMCU firmware. `sctools` is built as part of the [pkt-forwarder](https://gitlab.com/nlighten-systems/pkt-forwarder) project. You will need to load `sctools` onto your Gateway hardware (I.E. Raspberry Pi) and then run it via command line.

## Download Firmware

Firmware can be downloaded via the UART on the mPCIe connector. The bootloader uses XMODEM transfer. `sctools` can perform the download easly with the following command.

```
 $ sctools download -f app.bin_signed -s /dev/serial0 -r 17
```

The reset pin is pin 22 of the mPCIe connector. Your platform will need to connect the reset pin to a GPIO. When using a Raspberry Pi and RAK2287/RAK5146 Pi HAT, the reset pin is `17`.

## Receive Test

Put the Concentrator Card into continus receive mode and prints info about every packet received. 

```
 $ sctools rx --config conf.json.kompressor.US915.SPI 
----- 2023-05-08T00:54:07.495909+00:00 packet: 747d1476 -----
  freq: 904900391
  datr: SF7BW125
  snr : 1350
  tmst: 1561313651
  rssi_sig (raw): 172
  rssi_sig (dBm): -43.4
  rssi_chn (raw): 172
  gps time: 2023-05-08T00:54:25.210954959Z
  gps_pos : [43.5434116, -72.3927785]
  data: AG5fb9qT+YFgoWmSZYf5gWDA6H4nJZQ

----- 2023-05-08T00:54:07.673058+00:00 sig packet: 747d1476 -----
  sig: EpljJAeaxTU2GAxN8XcRnjRycklrYmIiQXzW8aYhD22ik3/SjPPVSWSGE+Gd84VVnqAj+G8WzClDsp3xE05BAQ
  verify: true

```

## Transmit Test

Test sending packets

```
$ ./sctools tx -f 915.2 -d SF7BW125 --config conf.json.kompressor.US915.SPI
Sending 915.2MHz SF7BW125 10dBm
sent
```


