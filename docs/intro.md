---
sidebar_position: 1
---

# What is a Secure Concentrator?

![](./img/kompressor_render01.jpg)

Secure Concentrator Card is a mPCIe LoRaWAN Concentrator based Semtech SX1303 baseband processor with on-board GPS receiver and Secure Microcontroller. Secure Concentrators digitally sign every recieved LoRa packet with a unique ED25519 cryptographic key making it ideal for use in DePIN projects such as Helium where proof of physical location is required.

The Secure Concentrator Card (codename: Kompressor) was originally developed by NLighten Systems and funded in part by a grant from the [Helium Foundation](http://www.helium.foundation). Kompressor is fully open-source [hardware](https://gitlab.com/nlighten-systems/kompressor) (licensed under CERN-OHL-W) and [firmware](https://gitlab.com/nlighten-systems/kompressor-firmware) (licensed under GPLv3). The project is certified open source by OSHWA [US002135](https://certification.oshwa.org/us002135.html).

Secure Concentrator is the key component of [Helium Improvment Proposal 72 (HIP-72)](https://github.com/helium/HIP/blob/main/0072-secure-concentrators.md)

## Kicad PCB Design

Secure Concentrator hardware is designed with Kicad (Open source CAD software). The hardware source files can be found here:
 [Offical NLighten Systems Hardware Project](https://gitlab.com/nlighten-systems/kompressor/)


# Hardware Architecture

![Hardware Arch](./img/hardware_block.png)

Secure Concentrator is based on Semtech's LoRa Corecell Gateway reference design. The major change involves the addition of a Secure MCU (SMCU) placed in between the communication path of the Host CPU and the SX1303. The SMCU's primary job is to cryptographically sign RF data received over the air.


